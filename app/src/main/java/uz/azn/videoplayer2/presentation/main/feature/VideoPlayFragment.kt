package uz.azn.videoplayer2.presentation.main.feature

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import uz.azn.videoplayer2.R
import uz.azn.videoplayer2.databinding.FragmentVideoPlayBinding

class VideoPlayFragment : Fragment(R.layout.fragment_video_play) {
    private lateinit var binding: FragmentVideoPlayBinding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    binding = FragmentVideoPlayBinding.bind(view)
    }

}