package uz.azn.videoplayer2.presentation.main

import android.app.AlertDialog
import android.app.DownloadManager
import android.content.BroadcastReceiver
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.database.Cursor
import android.net.Uri
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.View
import android.webkit.URLUtil
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import uz.azn.videoplayer2.R
import uz.azn.videoplayer2.databinding.FragmentVideoBinding
import uz.azn.videoplayer2.presentation.main.adapter.VideoAdapter
import uz.azn.videoplayer2.presentation.main.model.VideoModel
import java.io.File

class VideoFragment : Fragment(R.layout.fragment_video) {
    private lateinit var binding: FragmentVideoBinding
    private lateinit var builder: AlertDialog.Builder
    private lateinit var dialog: AlertDialog
    private lateinit var clipboardManager: ClipboardManager
    private val videoModels = mutableListOf<VideoModel>()
   private var downloadID: Long = 0
    private val videoAdapter by lazy {
        VideoAdapter(
            onItemClicked = {

            }
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentVideoBinding.bind(view)
        with(binding) {
            btnAdd.apply {
                setOnClickListener {
                    inputDialog()
                }
            }
        }

    }

    private fun inputDialog() {
        val view = layoutInflater.inflate(R.layout.input_url_dialog, null, false)
        builder = AlertDialog.Builder(requireContext())
            .setView(view)
            .setCancelable(false)

        dialog = builder.create()

        val btnPaste = view.findViewById<Button>(R.id.btn_paste)
        val etInput = view.findViewById<EditText>(R.id.et_input)
        val btnCancel = view.findViewById<Button>(R.id.btn_cancel)
        val btnDownload = view.findViewById<Button>(R.id.btn_download)

        btnPaste.setOnClickListener {
            try {
                clipboardManager =
                    requireContext().getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
                val charSequence = clipboardManager.primaryClip!!.getItemAt(0).text
                etInput.setText(charSequence)
            } catch (ex: Exception) {
                ex.printStackTrace()
            }

        }
        btnCancel.setOnClickListener {
            dialog.dismiss()
        }
        btnDownload.setOnClickListener {
            if (etInput.text.toString().endsWith(".mp4")) {
                downloadFile(etInput.text.toString())
                dialog.dismiss()
            } else {
                Toast.makeText(requireContext(), "Malumot hato kiritildi ", Toast.LENGTH_SHORT)
                    .show()
            }
        }


        dialog.show()
    }

    private fun downloadFile(url: String) {
        val filename = URLUtil.guessFileName(url, null, null)
        val downloadPath =
            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
                .absolutePath
        val file = File(downloadPath, filename)
        var request: DownloadManager.Request? = null
        request = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            DownloadManager.Request(Uri.parse(url))
                .setTitle(filename)
                .setDescription("Downloading")
                .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE)
                .setDestinationUri(Uri.fromFile(file))
                .setRequiresCharging(false)
                .setAllowedOverMetered(true)
                .setAllowedOverRoaming(true)
        } else {
            DownloadManager.Request(Uri.parse(url))
                .setTitle(filename)
                .setDescription("Downloading")
                .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE)
                .setDestinationUri(Uri.fromFile(file))
                .setAllowedOverMetered(true)
                .setAllowedOverRoaming(true)
        }
        val downloadManager =
            requireContext().getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
        downloadID = downloadManager.enqueue(request)

        val videoModel = VideoModel(
            1,
            downloadID,
            filename,
            "",
            "0",
            "Downloading",
            "0",
            false
        )
        videoModels.add(videoModel)
        videoAdapter.setData(requireContext(), videoModels as ArrayList<VideoModel>)
        with(binding) {

            recyclerView.apply {
                layoutManager = LinearLayoutManager(requireContext())
                adapter = videoAdapter

            }
        }


    }

    private fun getStatusMessage(cursor: Cursor): String {
        var msg = "-"
        msg = when (cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS))) {
            DownloadManager.STATUS_FAILED -> "Failed"
            DownloadManager.STATUS_PAUSED -> "Paused"
            DownloadManager.STATUS_RUNNING -> "Running"
            DownloadManager.STATUS_SUCCESSFUL -> "Completed"
            DownloadManager.STATUS_PENDING -> "Pending"
            else -> "Unknown"
        }
        return msg
    }

    private fun bytesIntoHumanReadable(bytes: Long): String? {
        val kilobyte: Long = 1024
        val megabyte = kilobyte * 1024
        val gigabyte = megabyte * 1024
        val terabyte = gigabyte * 1024
        return if (bytes in 0 until kilobyte) {
            "$bytes B"
        } else if (bytes in kilobyte until megabyte) {
            (bytes / kilobyte).toString() + " KB"
        } else if (bytes in megabyte until gigabyte) {
            (bytes / megabyte).toString() + " MB"
        } else if (bytes in gigabyte until terabyte) {
            (bytes / gigabyte).toString() + " GB"
        } else if (bytes >= terabyte) {
            (bytes / terabyte).toString() + " TB"
        } else {
            "$bytes Bytes"
        }
    }

    inner class DownloadStatusTask : AsyncTask<String, String, String>() {

        private var downloadModel: VideoModel? = null
        private fun DownloadStatusTask(downloadModel: VideoModel) {
            this.downloadModel = downloadModel
        }

        override
        fun doInBackground(vararg strings: String?): String? {
            downloadFileProcess(strings[0]!!)
            return null
        }

        private fun downloadFileProcess(downloadId: String) {
            val downloadManager =
                requireContext().getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
            var downloading = true
            while (downloading) {
                val query = DownloadManager.Query()
                query.setFilterById(downloadId.toLong())
                val cursor = downloadManager.query(query)
                cursor.moveToFirst()
                val bytes_downloaded =
                    cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR))
                val total_size =
                    cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_TOTAL_SIZE_BYTES))
                if (cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS)) == DownloadManager.STATUS_SUCCESSFUL) {
                    downloading = false
                }
                val progress = (bytes_downloaded * 100L / total_size).toInt()
                val status = getStatusMessage(cursor)
                publishProgress(
                    *arrayOf(
                        progress.toString(),
                        bytes_downloaded.toString(),
                        status
                    )
                )
                cursor.close()
            }
        }
    }

    private fun handlePdfFile(intent: Intent) {
        val pdffile =
            intent.getParcelableExtra<Uri>(Intent.EXTRA_STREAM)
        if (pdffile != null) {
            Log.d("Pdf File Path : ", "" + pdffile.path)
        }
    }

    private fun handleImage(intent: Intent) {
        val image =
            intent.getParcelableExtra<Uri>(Intent.EXTRA_STREAM)
        if (image != null) {
            Log.d("Image File Path : ", "" + image.path)
        }
    }

    private fun handleTextData(intent: Intent) {
        val textdata = intent.getStringExtra(Intent.EXTRA_TEXT)
        if (textdata != null) {
            Log.d("Text Data : ", "" + textdata)
            downloadFile(textdata)
        }
    }

    private fun handleMultipleImage(intent: Intent) {
        val imageList =
            intent.getParcelableArrayListExtra<Uri>(Intent.EXTRA_STREAM)
        if (imageList != null) {
            for (uri in imageList) {
                Log.d("Path ", "" + uri.path)
            }
        }
    }
    fun runTask(downloadStatusTask: DownloadStatusTask, id: String) {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                downloadStatusTask.executeOnExecutor(
                    AsyncTask.THREAD_POOL_EXECUTOR,
                    *arrayOf(id)
                )
            } else {
                downloadStatusTask.execute(*arrayOf(id))
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }

}