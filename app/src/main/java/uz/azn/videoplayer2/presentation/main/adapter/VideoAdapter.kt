package uz.azn.videoplayer2.presentation.main.adapter

import android.content.ContentValues
import android.content.Context
import android.net.Uri
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import uz.azn.videoplayer2.presentation.main.model.VideoModel
import uz.azn.videoplayer2.databinding.VideoItemViewHolderBinding as VideoBinding

class VideoAdapter(val onItemClicked: (String) -> Unit) :
    RecyclerView.Adapter<VideoAdapter.VideoViewHolder>() {
    private lateinit var context: Context
    private val elements = mutableListOf<VideoModel>()
    override fun onCreateViewHolder(
        parent: ViewGroup, viewType: Int
    ): VideoViewHolder =
        VideoViewHolder(VideoBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun getItemCount(): Int = elements.size

    override fun onBindViewHolder(holder: VideoViewHolder, position: Int) {
        holder.onBind(elements[position])
    }

    inner class VideoViewHolder(private val binding: VideoBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun onBind(element: VideoModel) {
            with(binding) {
                fileTitle.text = element.fileTitle
                fileStatus.text = element.status
                fileProgress.progress = Integer.parseInt(element.progress)
                fileSize.text = "Download: ${element.fileSize}"

                if (element.isPaused) {
                    // agar pause bosilganda
                    btnPauseResume.text = "Resume"
                } else {
                    btnPauseResume.text = "Pause"
                }

                if (element.status.equals("Resume", true)) {
                    fileStatus.text = "Running"
                }

                btnPauseResume.apply {
                    setOnClickListener {
                        if (element.isPaused) {
                            element.isPaused = false
                            btnPauseResume.text = "Pause"
                            element.status = "Resume"
                            fileStatus.text = "Running"
                            if (!resumeDownload(element)) {
                                Toast.makeText(context, "Failed to Resume", Toast.LENGTH_SHORT)
                                    .show()
                            }
                            notifyItemChanged(adapterPosition)
                        } else {
                            element.isPaused = true
                            btnPauseResume.text = "Resume"
                            element.status = "Pause"
                            fileStatus.text = "Pause"
                            if (!pauseDownload(element)) {
                                Toast.makeText(context, "Failed to Resume", Toast.LENGTH_SHORT)
                                    .show()
                            }
                            notifyItemChanged(adapterPosition)
                        }
                    }
                }
            }


        }
    }

    fun setData(mContext: Context, elementVideo: ArrayList<VideoModel>) {
        elements.apply { clear();addAll(elementVideo) }
        this.context = mContext
        notifyDataSetChanged()
    }

    private fun pauseDownload(videoModel: VideoModel): Boolean {
        var updatedRow = 0
        val contentValues = ContentValues()
        contentValues.put("control", 1)
        try {
            updatedRow = context.contentResolver.update(
                Uri.parse("content://downloads/my_downloads"),
                contentValues,
                "title=?",
                arrayOf(videoModel.fileTitle)
            )
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return 0 < updatedRow
    }

    private fun resumeDownload(videoModel: VideoModel): Boolean {
        var updatedRow = 0
        val contentValues = ContentValues()
        contentValues.put("control", 0)
        try {
            updatedRow = context.contentResolver.update(
                Uri.parse("content://downloads/my_downloads"),
                contentValues,
                "title=?",
                arrayOf(videoModel.fileTitle)
            )
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
        return 0 < updatedRow
    }

    fun changeItem(downloadId: Long) {
        for ((i, element) in elements.withIndex()) {
            if (downloadId == element.downloadId) {
                notifyItemChanged(i)

            }
        }

    }


}